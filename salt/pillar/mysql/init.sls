mysql:
  # Server settings
  server:
    root_password: 'vagrant'
    user: root

  # Databases
  database:
    - eshop
    - pmadb
  schema:
    eshop:
      load: True
      source: salt://mysql/files/eshop.sql
    pmadb:
      load: True
      source: salt://phpmyadmin/files/pmadb.sql

  # Users
  user:
    - name: konzmod
      password: 'konzmod'
      host: localhost
      databases:
      - database: eshop
        grants: ['all privileges']

    - name: phpmyadmin
      password: 'secret'
      host: localhost
      databases:
      - database: pmadb
        grants: ['select', 'insert', 'update', 'delete']
