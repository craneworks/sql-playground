{% from "apache/map.jinja" import apache with context %}

phpmyadmin:
    pkg:
        - installed

{{ apache.confdir }}/phpmyadmin.conf:
    file.symlink:
        - target: /etc/phpmyadmin/apache.conf
        - require:
                - pkg: phpmyadmin
                - pkg: apache
                - pkg: mysql
        - watch_in:
            - module: apache-reload

/etc/phpmyadmin/config-db.php:
    file.managed:
        - source: salt://phpmyadmin/files/config-db.php
        - user: root
        - group: root
        - mode: 644
        - require:
            - pkg: phpmyadmin
        - watch_in:
            - module: apache-reload
