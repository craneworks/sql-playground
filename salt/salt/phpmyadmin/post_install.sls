{%- set mysql_root_pass = salt['pillar.get']('mysql:server:root_password', 'somepass') %}

/tmp/pma.sql:
  file.managed:
    - source: salt://phpmyadmin/files/pma.sql

mysql -u root -p{{ mysql_root_pass }} < /tmp/pma.sql:
  cmd:
    - run
    - require:
        - file: /tmp/pma.sql
        - pkg: phpmyadmin
