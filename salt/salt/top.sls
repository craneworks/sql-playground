base:
  '*':
    - mysql
    - mysql.client
    - mysql.server
    - mysql.database
    - mysql.user
    - apache
    - apache.php5
    - phpmyadmin
    - phpmyadmin.post_install
